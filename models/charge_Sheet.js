    const mongoose = require("mongoose")

    const chargeSheetSchema = new mongoose.Schema({

        charge_id: {
            type: Number, 
            required: true,
            unique: true
        },
        appointment_date: {
            type: Date,
            required: true
        },
        patient_id: {
            type: Number,
            required: true
        },
        doctor_id: {
            type: Number,
            required: true
        },
        bill_id: {
            type: Number,
            
            
        },
        charge: { 
            type: mongoose.Decimal128,
            required: true,
            min: 0,
            max: 99999.99
        }
    })

    const chargeSheet= mongoose.model('ChargeSheet',chargeSheetSchema)
    module.exports=chargeSheet


