
# Setting up a CI/CD Pipeline with GitLab


The objective of this assignment is to get familiarize  with setting up a Continuous Integration/Continuous Deployment (CI/CD) pipeline using GitLab CI/CD and Jenkins. You will create a simple pipeline that automates the deployment process of a sample web / desktop application.




## 
## Setup Gitlab and initialise a project repo to add the Project Files

![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/0f7bbb88-6570-4a14-a4ad-ba7678753c33)


#
## Adding .gitlab-ci.yml file for project configurations

![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/1f3132b5-98d0-4397-9387-ab0779529702)

#

## Starting the Pipeline based on yml file

![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/95ea3ce4-fc22-4315-844b-fc96e32cbeb1)

#

## Building the pipeline in stages to carry out deployement

![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/1c0e210f-21b3-4b23-917b-a6924430f67d)

#

## Building the pipeline in stages to carry out deployement

![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/1c0e210f-21b3-4b23-917b-a6924430f67d)


#
## Build Job succeded

![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/7162b88d-86d7-47b8-a389-5ff369b8c7b3)


#
## Server running in background and waiting for changes 

![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/c9a58715-78ca-41e6-8a82-0428134a9680)


#
#
# 


# Connect the GitLab repository to Jenkins server and build the application.

## Starting Jenkins Service Locally

![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/01ca4f42-7a02-4963-a5eb-cc21328d16f7)

#
## Jenkins Dashboard

![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/6c227a9d-f414-4d90-aa90-b781c34b84f8)

#
## Connect Gitlab repo with Jenkins

![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/587a6a55-2dbc-4cc3-991f-c77aa70408dc)

#
## Automatically Build the Gitlab Project
![App Screenshot](https://github.com/AayushAnand07/E-Learning/assets/41218074/3d9d8af4-6e88-4644-848f-c58148ada54a)








